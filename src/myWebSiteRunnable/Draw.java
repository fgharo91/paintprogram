package myWebSiteRunnable;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JApplet;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import GraphicalUserInterfaces.InternalDrawingPanel;
import GraphicalUserInterfaces.InternalDrawingWindow;

/**
 *  Assignment #: 2
 *  Name: 		  Gabriel
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: This class defines the JApplet for viewing purposes on my website.
 */
public class Draw extends JApplet {
	
	// The drawing panel.
	private final InternalDrawingPanel drawingPanel = new InternalDrawingPanel();
		
	public void init() {
		this.setSize(400, 400);
		setGUIComponents();
	}
	
	/**
	 * Composes the GUI components in the container MainWindow.
	 */
	private void setGUIComponents() {
		this.getContentPane().add(drawingPanel);		
	}
	

}
