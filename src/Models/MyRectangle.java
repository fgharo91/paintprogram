package Models;

import java.awt.Color;
import java.awt.Graphics;

/**
 *  Assignment #: 2
 *  Name: 		  Gabriel
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: Sub class of MyShape. Defines a rectangle to be drawn to 
 *  a canvas or jpanel.
 */
public class MyRectangle extends MyShape {
	
	/**
	 * Sets up a Rectangle with the coordinates (x1, y1) to (x2, y2).
	 * @param x1 the x coordinate of the upper left corner point of this rectangle.
	 * @param y1 the y coordinate of the upper left corner point of this rectangle.
	 * @param x2 the x coordinate of the lower right corner point of this rectangle.
	 * @param y2 the y coordinate of the lower right corner point of this rectangle.
	 */
	public MyRectangle(int x1, int y1, int x2, int y2, Color color) {
		super( x1,  y1, x2, y2, color);
	}

	/**
	 * Paints the rectangle onto a canvas or jpanel.
	 * @param g A graphics object to draw a visual representation of this rectangle.
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		// Draws rectangle filled.
		if(isFilled)
		g.fillRect(x1, y1, Math.abs(x2 - x1), Math.abs(y2 - y1));
		else // Draws an outline of the rectangle.
			g.drawRect(x1, y1, Math.abs(x2 - x1), Math.abs(y2 - y1));

	}
}
