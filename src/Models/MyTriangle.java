package Models;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class MyTriangle extends MyShape{
	int x3,y3;

	public MyTriangle(int x1, int y1, int x2, int y2, int x3, int y3, Color color) {
		super(x1, y1, x2, y2, color);
		this.x3 = x3;
		this.y3 = y3;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		
		if(isFilled)
			g.fillPolygon(new Polygon(new int[]{x1,x2,x3},new int[]{y1,y2,y3}, 3));
		else
			g.drawPolygon(new Polygon(new int[]{x1,x2,x3},new int[]{y1,y2,y3}, 3));
		
		// TODO Auto-generated method stub
		
	}

}
