package Models;

import java.awt.Color;
import java.awt.Graphics;

/**
 *  Assignment #: 2
 *  Name: 		  Gabriel
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: Sub class of MyShape. Defines a line to be drawn to 
 *  a canvas or jpanel.
 */
public class MyLine extends MyShape {

	/**
	 * Sets up a line from coordinates (x1, y1) to (x2, y2).
	 * @param x1 the x coordinate of the upper left corner point of this shapes start point.
	 * @param y1 the y coordinate of the upper left corner point of this shapes start point.
	 * @param x2 the x coordinate of the lower right corner point of this shapes end point.
	 * @param y2 the y coordinate of the lower right corner point of this shapes end point.
	 */
	public MyLine(int x1, int y1, int x2, int y2, Color color) {
		super(x1, y1, x2, y2, color);
	}

	/**
	 * Paints the line onto a canvas or jpanel.
	 * @param g A graphics object to draw a visual representation of this line.
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		// draws line.
		g.drawLine(x1, y1, x2 , y2);

	}

}
