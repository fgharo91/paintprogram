package Models;
import java.awt.Color;
import java.awt.Graphics;

/**
 *  Assignment #: 2
 *  Name: 		  Frank Haro
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: MyShape is the parent class of MyLine, MyOval, and MyRectangle.
 *  It serves as the general definition of what it means to be a shape. It also
 *  declares a draw method for sub classes to implement in order to draw a visual
 *  representation on a canvas of the window.
 */
public abstract class MyShape {
	// All shapes that are outlined or filled
	// have a rectangle that defines its boundaries.
	
	// The coordinates (x1, y1) is the upper left corner
	// point of this rectangle boundary and (x2, y2) is
	// the lower right corner point of this rectangle boundary.
	protected int x1,x2,y1,y2;
	
	// the boolean isFilled is a flag that tracks
	// whether the shape is being drawn as an outline of the
	// shape or drawn as a filled shape.
	protected boolean isFilled;
	
	// the color of this shape.
	protected Color color;
	
	/**
	 * Sets up a shape with the coordinates defining the boundary rectangle of this shape.
	 * @param x1 the x coordinate of the upper left corner point of this shapes boundary rectangle.
	 * @param y1 the y coordinate of the upper left corner point of this shapes boundary rectangle.
	 * @param x2 the x coordinate of the lower right corner point of this shapes boundary rectangle.
	 * @param y2 the y coordinate of the lower right corner point of this shapes boundary rectangle.
	 */
	public MyShape(int x1, int y1, int x2, int y2, Color color) {
		this.x1 = x1; this.y1 = y1; this.x2 = x2; this.y2 = y2;
		this.color = color;
	}
	
	/**
	 * Declared abstract method draw for subclasses of MyShape
	 * to implement.
	 * @param g An object used to draw on a java canvas or JPanel.
	 */
	public abstract void draw( Graphics g );

	
	/**
	 * Sets whether the shape is logically drawn filled or not drawn filled.
	 * @param fill Can be true or false. If fill is true we say that the shape should 
	 * be drawn filled. If fill is false we say that the shape should be drawn as an outline.
	 */
	public void setFilled(boolean fill) {
		isFilled = fill;
	}
	
}

