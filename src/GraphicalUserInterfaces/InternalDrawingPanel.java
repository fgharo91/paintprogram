package GraphicalUserInterfaces;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *  Assignment #: 2
 *  Name: 		  Gabriel
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: This class defines the panel for the JInternalFrames.
 */
@SuppressWarnings("serial")
public class InternalDrawingPanel extends JPanel implements ItemListener
{
	// Dimensions of the JPanel.
	private final int width = 400, height = 400;	
	// Panels of this JPanel
	private final JPanel northSubPanel = new JPanel(new FlowLayout());
	private final JPanel southPanel = new JPanel(new GridLayout(1,2));
	private final JPanel southSubPanel1 = new JPanel(new GridLayout(3,1));
	private final JPanel southSubPanel2 = new JPanel(new GridLayout(2,1));
	private final TextPanel title1= new TextPanel(new Font(Font.SERIF,Font.ROMAN_BASELINE,30),"Current"),
							title2= new TextPanel(new Font(Font.SERIF,Font.ROMAN_BASELINE,30),"Color");
	
	// Components for selecting a shape to draw, indicating if filled, and labels to let the user know what slider 
	// correspond to which colors.
	private final JComboBox<String> shapesSelection = new JComboBox<String>(new String[]{"Rectangle", "Line", "Oval", "Triangle"});
	private final JButton undoButton= new JButton("Undo");
	private final JButton eraseButton= new JButton("Erase");
	private final JButton saveButton=new JButton("Save");
	private final JCheckBox checkBox = new JCheckBox("Filled");

	// Sliders for indicating the range of the color.
	private final JSlider slider1 = new JSlider(0,255), 
			slider2 = new JSlider(0,255), 
			slider3 = new JSlider(0,255);

	// Canvas for drawing.
	private final DrawingCanvas drawingBoard = new DrawingCanvas(slider1.getValue(), slider2.getValue(), slider3.getValue());
	/**
	 * Calls the appropriate methods to set up this GUI.
	 */
	public InternalDrawingPanel() {
		setPanelComponents();
		setEventHandlers();
	}


	public void paint(Graphics paintTool){
		super.paint(paintTool);
		title1.repaint();
		title2.repaint();
	}

	/**
	 * Returns the drawing canvas of this internal drawing panel.
	 */
	public DrawingCanvas getCanvas(){
		return drawingBoard;
	}

	/**
	 * Composes the GUI components in the container MainWindow.
	 */
	private void setPanelComponents() {

		// Stylize components
		// South sub panel
		southPanel.setPreferredSize(new Dimension(width-20, height/4 ));
		southSubPanel1.setPreferredSize(new Dimension(southPanel.getWidth()/2, southPanel.getHeight()));
		southSubPanel2.setPreferredSize(new Dimension(southPanel.getWidth()/2, southPanel.getHeight() ));	

		title1.setSize(southSubPanel2.getWidth(),southSubPanel2.getHeight()/2);
		title2.setSize(southSubPanel2.getWidth(),southSubPanel2.getHeight()/2);
		title1.setColor(new Color(slider1.getValue(), slider2.getValue(), slider3.getValue()));
		title2.setColor(new Color(slider1.getValue(), slider2.getValue(), slider3.getValue()));
		
		drawingBoard.setPreferredSize(new Dimension(width-20, height-150));
		
		slider1.setBackground(Color.red);
		slider2.setBackground(Color.green);
		slider3.setBackground(Color.blue);
		
		slider1.setForeground(Color.white);
		slider2.setForeground(Color.white);
		slider3.setForeground(Color.white);
		
		slider1.setFont(new Font(Font.SERIF,Font.ROMAN_BASELINE,12));
		slider2.setFont(new Font(Font.SERIF,Font.ROMAN_BASELINE,12));
		slider3.setFont(new Font(Font.SERIF,Font.ROMAN_BASELINE,12));
		slider1.setMajorTickSpacing(255);
		slider2.setMajorTickSpacing(255);
		slider3.setMajorTickSpacing(255);
		slider1.setPaintLabels(true);
		slider2.setPaintLabels(true);
		slider3.setPaintLabels(true);
		
		
		// Compose North sub panel
		northSubPanel.add(shapesSelection);
		northSubPanel.add(saveButton);
		northSubPanel.add(undoButton);
		northSubPanel.add(eraseButton);
		northSubPanel.add(checkBox);


		// Compose South sub panel
		southSubPanel1.add(slider1);
		southSubPanel1.add(slider2);
		southSubPanel1.add(slider3);
		southSubPanel2.add(title1);
		southSubPanel2.add(title2);
		southPanel.add(southSubPanel1);
		southPanel.add(southSubPanel2);


		// Add North and South Sub Panels to Main panel.
		this.add(northSubPanel, BorderLayout.NORTH);
		this.add(drawingBoard, BorderLayout.CENTER);
		this.add(southPanel, BorderLayout.SOUTH);
	}


	/**
	 * Sets up the event handlers to the GUI components.
	 */
	private void setEventHandlers() {
		// Sets the listener for the shapesSelection.
		shapesSelection.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				JComboBox temp = (JComboBox) event.getSource();
				drawingBoard.setDrawingShapeChoice(temp.getSelectedIndex());
			}

		});

		ButtonListener buttonListener = new ButtonListener();
		undoButton.addActionListener(buttonListener);
		eraseButton.addActionListener(buttonListener);
		saveButton.addActionListener(buttonListener);

		// Set the slider listener for the sliders.
		SliderListener sliderListener = new SliderListener();

		slider1.addChangeListener(sliderListener);		
		slider2.addChangeListener(sliderListener);		
		slider3.addChangeListener(sliderListener);

		checkBox.setSelected(false);
		checkBox.addItemListener(this);

		drawingBoard.setCanvasListener();

	}

	/**
	 * A listener method for the checkbox.
	 */
	@Override
	public void itemStateChanged(ItemEvent event) {
		// TODO Auto-generated method stub
		if(checkBox.isSelected()){
			drawingBoard.setFill(true);
			drawingBoard.repaint();
		}else{
			drawingBoard.setFill(false);
			drawingBoard.repaint();
		}

	}


	//====================================================================================
	/**
	 * * Serves as a listener class for the JSliders.
	 * @author Frank Haro
	 */
	//====================================================================================
	private class SliderListener implements ChangeListener
	{
		/**
		 * After a slider is moved the ChangeEvent is fired and
		 * this method is called to handle it.
		 */
		public void stateChanged(ChangeEvent event)
		{
			// Get the slider that was activated.
			JSlider temp = (JSlider) event.getSource();		

			// display the color for the red, green, and blue words.
			title1.setColor(new Color(slider1.getValue(), slider2.getValue(), slider3.getValue()));
			title2.setColor(new Color(slider1.getValue(), slider2.getValue(), slider3.getValue()));

			// Also update the color the shapes are being painted with on the drawingBoard.
			drawingBoard.setRGB(slider1.getValue(), slider2.getValue(), slider3.getValue());
			temp.getParent().repaint();
		} 
	} //end of SliderListener class

	//====================================================================================
	/**
	 * * Serves as a listener class for the JButtons
	 * @author Frank Haro
	 */
	//====================================================================================
	private class ButtonListener implements ActionListener
	{	
		public void actionPerformed(ActionEvent event)
		{	// Listens for what button was pressed, Undo or Erase. Then, either removes 
			// a rectangle or clears the canvas but saves the rectList in a temp rectangle
			// array just in case the user clicks Undo to get back all of the rectangles.
			if(event.getSource() == undoButton){
				drawingBoard.undoShape();
			}else{
				if(event.getSource() == eraseButton)
					drawingBoard.eraseCanvas();
				else{
					// The code below was taken more or less from an Oracle tutorial code.
					// http://docs.oracle.com/javase/tutorial/2d/images/saveimage.html
					// Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
					BufferedImage image = (BufferedImage) getCanvas().getSavableImage();
					getCanvas().paint(image.createGraphics());
					File saveFile = new File("drawing.gif");
					JFileChooser chooser = new JFileChooser();
					chooser.setSelectedFile(saveFile);
					int rval = chooser.showSaveDialog(null);
					if (rval == JFileChooser.APPROVE_OPTION) {
						saveFile = chooser.getSelectedFile();

						/* Write the filtered image in the selected format,
						 * to the file chosen by the user.
						 */
						try {
							ImageIO.write(image, "gif", saveFile);
						} catch (IOException ex) {
							JOptionPane.showMessageDialog(drawingBoard.getParent(), ex.getMessage());
						}
					}
				}

			}

		} // end of actionPerformed

	} // end of ButtonListener

}
/**
 * The code below was taken more or less from an Oracle tutorial code.
 * http://docs.oracle.com/javase/tutorial/2d/text/fonts.html
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 */
class TextPanel extends JPanel {

	String text;
	Color color;

	public TextPanel(Font font, String text) {
		super.setFont(font);
		this.text = text;
	}

	public void setColor(Color color) {
		this.color = color;
		repaint();
	}
	public Dimension getPreferredSize() {
		return new Dimension(500,200);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		FontMetrics metrics = g.getFontMetrics();
		int x = getWidth()/2 - metrics.stringWidth(text)/2;
		int y = getHeight()/2;

		g.setColor(color);
		g.setFont(getFont());
		g.drawString(text, x,y);
	}
}
