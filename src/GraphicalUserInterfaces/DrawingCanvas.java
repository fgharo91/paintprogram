package GraphicalUserInterfaces;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.EventObject;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import Models.MyLine;
import Models.MyOval;
import Models.MyRectangle;
import Models.MyShape;
import Models.MyTriangle;

/**
 *  Assignment #: 2
 *  Name: 		  Frank Haro
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: This class defines the drawing like canvas JPanel located in the center of
 *  the InternalDrawingPanel for the InternalDrawingWindows(JInternalFrames).
 */
@SuppressWarnings("serial")
public class DrawingCanvas extends JPanel {
	
	// An ArrayList to hold all the model shapes created by the user.
	private ArrayList<MyShape> shapeList;	
	private ArrayList<MyShape> tempShapeList;
	private int drawingChoice;	
	private Color currentColor;
	
	// Variables for the drawing.
	private boolean dragging = false, fillShapes = false;
//   	private Point pressedPoint;
//   	private Point releasedPoint;
   	private Point draggedPoint;
   	private Point clickedPoint1;
   	private Point clickedPoint2;
   	private Point clickedPoint3;
   	

   	/**
   	 * Initializes the inner array to hold the shapes drawn and initializes the color
   	 * the shapes will be drawn with.
   	 * @param redValue - The red value to be used in the color to draw shapes with.
   	 * @param greenValue - The green value to be used in the color to draw shapes with.
   	 * @param blueValue - The blue value to be used in the color to draw shapes with.
   	 */
	public DrawingCanvas(int redValue, int greenValue, int blueValue) {
		setRGB(redValue, greenValue, blueValue);
		shapeList = new ArrayList<MyShape>();
		tempShapeList = new ArrayList<MyShape>();
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		
	}

	/**
	 * @param choice - if the JComboBox is set to Rectangle (0) we will
	 * set the drawingChoice variable. The same goes for the other shapes.
	 */
	public void setDrawingShapeChoice(int choice){
		drawingChoice = choice;
	}

	/**
	 * 
	 * @param fill - Sets the logical variable for the Drawing Canvas to know
	 * whether we are drawing filled shapes or outlined shapes.
	 */
	public void setFill(boolean fill) {
		fillShapes = fill;
	}

	/**
	 * Sets the color to paint with. Then calls the repaint() to
	 * make sure the shapes drawn reflect the new color.
	 * @param redValue - the red value in an rgb color.
	 * @param greenValue - the green value in an rgb color.
	 * @param blueValue - the blue value in an rgb color.
	 */
	public void setRGB(int redValue, int greenValue, int blueValue) {
		currentColor = new Color(redValue, greenValue, blueValue); 
		this.repaint();
	}

	/**
	 * Sets the listener of this DrawingCanvas in order to handle mouse events.
	 * Clicking and Dragging and releasing mouse clicks..
	 */
	public void setCanvasListener(){
		this.addMouseListener(new PointListener());
		this.addMouseMotionListener(new PointListener());
	}

	/**
	 * 
	 * @return An image of the canvas to be saved to disk.
	 */
	public BufferedImage getSavableImage() {
		return new BufferedImage(this.getSize().width, this.getSize().height,BufferedImage.TYPE_INT_RGB);
	}
	
	public void undoShape(){
		if(shapeList.size() > 0){
			shapeList.remove(shapeList.size() - 1);
			this.repaint();
		}else{
			// What is this for? If we undo an erase?
			for(int i = 0; i < tempShapeList.size(); i++){
				shapeList.add(tempShapeList.get(i));
			}
			tempShapeList.clear();
			this.repaint();
		}
	}
	
	public void eraseCanvas() {
		for(int i = 0; i < shapeList.size(); i++){
			tempShapeList.add(shapeList.get(i));
		}
		shapeList.clear();
		this.repaint();
	}

	/**
	 * This paint method will redraw the shapes already made and also 
	 * draw the call appropriate methods for drawing outlines of the shape
	 * as the user creates them.
	 * @param paintTool - used to paint shapes with.
	 */
	@Override
	public void paint(Graphics paintTool) {
		
		super.paint(paintTool);
		
		// Each Drawing canvas will have a white background. 
		setBackground(Color.white);

        // for loop to repaint already made shapes onto canvas.
        for(int i = 0; i < shapeList.size(); i++){        	
        	//shapeList.get(i).setFilled(fillShapes);
        	shapeList.get(i).draw(paintTool);
        }
        
        
        paintTool.setColor(currentColor);
        
        // if statements for drawing up outline.
		 if(dragging){
	    		// Call appropriate method to draw outline
			 	// as user clicked and drags the mouse.
			 	
			 switch(drawingChoice){
			 //rectangle was selected.
			 case 0:
				 drawingRectangleAnimation(paintTool);
				 break;
			 //line was selected.
			 case 1:
				 drawingLineAnimation(paintTool);
				 break;
			 //oval was selected.
			 case 2:
				 drawingOvalAnimation(paintTool); 
				 break;
			 //triangle was selected.
			 case 3:
				 drawingTriangleAnimation(paintTool);					 
				 break;
			 }
		 }

	}

	/**
	 * Draws an oval outline for the drawing animation effect.
	 * @param paintTool - To draw shape with.
	 */
	private void drawingOvalAnimation(Graphics paintTool) {
		// When oval is drawn by dragging from point pressed towards lower 
		// right of canvas.
//		if(pressedPoint.x < draggedPoint.x && pressedPoint.y < draggedPoint.y){
//			if(fillShapes)
//			paintTool.fillOval(pressedPoint.x, pressedPoint.y,
//			Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			else paintTool.drawOval(pressedPoint.x, pressedPoint.y,
//					Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			
//			return;
//		}
//		// When  oval outline is drawn by dragging from point pressed towards lower 
//		// left of canvas.
//		if(pressedPoint.x > draggedPoint.x && pressedPoint.y < draggedPoint.y){
//			if(fillShapes)
//        	paintTool.fillOval(draggedPoint.x, pressedPoint.y,
//        	Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			else paintTool.drawOval(draggedPoint.x, pressedPoint.y,
//		        	Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			
//			return;
//		}
//		// When  oval outline is drawn by dragging from point pressed towards upper 
//		// right of canvas.
//		if(pressedPoint.x < draggedPoint.x && pressedPoint.y > draggedPoint.y){
//			if(fillShapes)
//        	paintTool.fillOval(pressedPoint.x, draggedPoint.y,
//        	Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			else paintTool.drawOval(pressedPoint.x, draggedPoint.y,
//		        	Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			
//			return;
//		}
//		// When  oval outline is drawn by dragging from point pressed towards upper 
//		// left of canvas.
//		if(pressedPoint.x > draggedPoint.x && pressedPoint.y > draggedPoint.y){
//			if(fillShapes)
//        	paintTool.fillOval(draggedPoint.x, draggedPoint.y,
//        	Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			else paintTool.drawOval(draggedPoint.x, draggedPoint.y,
//		        	Math.abs(draggedPoint.x - pressedPoint.x), Math.abs(draggedPoint.y - pressedPoint.y));
//			
//			return;
//		} 
		
		if(clickedPoint1 != null && draggedPoint != null){
			// logic that works:
			// When oval is drawn by dragging from point pressed towards lower 
			// right of canvas.
			if(clickedPoint1.x < draggedPoint.x && clickedPoint1.y < draggedPoint.y){
				if(fillShapes)
					paintTool.fillOval(clickedPoint1.x, clickedPoint1.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
					else paintTool.drawOval(clickedPoint1.x, clickedPoint1.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
				return;
			}
			// When  oval outline is drawn by dragging from point pressed towards lower 
			// left of canvas.
			if(clickedPoint1.x > draggedPoint.x && clickedPoint1.y < draggedPoint.y){
				if(fillShapes)
		        	paintTool.fillOval(draggedPoint.x, clickedPoint1.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
					else paintTool.drawOval(draggedPoint.x, clickedPoint1.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
				return;
			}
			// When  oval outline is drawn by dragging from point pressed towards upper 
			// right of canvas.
			if(clickedPoint1.x < draggedPoint.x && clickedPoint1.y > draggedPoint.y){
				if(fillShapes)
		        	paintTool.fillOval(clickedPoint1.x, draggedPoint.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
					else paintTool.drawOval(clickedPoint1.x, draggedPoint.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));

				return;
			}
			// When  oval outline is drawn by dragging from point pressed towards upper 
			// left of canvas.
			if(clickedPoint1.x > draggedPoint.x && clickedPoint1.y > draggedPoint.y){
				if(fillShapes)
		        	paintTool.fillOval(draggedPoint.x, draggedPoint.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
					else paintTool.drawOval(draggedPoint.x, draggedPoint.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));

				return;
			} 
		}
		
	}

	/**
	 * Draws a line outline for the drawing animation effect.
	 * @param paintTool - To draw shape with.
	 */
	private void drawingLineAnimation(Graphics paintTool) {
		if(clickedPoint1 != null && draggedPoint != null)
		paintTool.drawLine(clickedPoint1.x, clickedPoint1.y, draggedPoint.x , draggedPoint.y);

	}
	
	/**
	 * Draws a triangle outline for the drawing animation effect.
	 * dragged Point will not be updated because mouseDragged will not be listening
	 */
	private void drawingTriangleAnimation(Graphics paintTool) {
		 // drawing the base of the triangle.
		 if(clickedPoint1 != null && clickedPoint2 == null && clickedPoint3 == null){
			 paintTool.drawLine(clickedPoint1.x, clickedPoint1.y, draggedPoint.x , draggedPoint.y);
			 
			 return;
		 }else{
			 // drawing the legs of the triangle.
			 if(clickedPoint1 != null && clickedPoint2 != null && clickedPoint3 == null && !fillShapes){
				 // draw the base.
				 paintTool.drawLine(clickedPoint1.x, clickedPoint1.y, clickedPoint2.x , clickedPoint2.y);
				 // draw the legs
				 paintTool.drawLine(clickedPoint1.x, clickedPoint1.y, draggedPoint.x , draggedPoint.y);
				 paintTool.drawLine(clickedPoint2.x, clickedPoint2.y, draggedPoint.x , draggedPoint.y);
				 
				 return;
				 
			 }else{
				 // drawing the legs with the triangle along with a filling.
				 if(clickedPoint1 != null && clickedPoint2 != null && clickedPoint3 == null && fillShapes)
				 paintTool.fillPolygon(new Polygon(new int[]{clickedPoint1.x,clickedPoint2.x,draggedPoint.x},
						 	   			   new int[]{clickedPoint1.y,clickedPoint2.y,draggedPoint.y}, 3));
				 
				 return;
				 
			 }
		 }
		
	}

	/**
	 * Draws a rectangle outline for the drawing animation effect.
	 * @param paintTool - To draw shape with.
	 */
	public void drawingRectangleAnimation(Graphics paintTool) {
		// When rectangle is drawn by dragging from point pressed towards lower 
		// right of canvas.
		if(clickedPoint1 != null && draggedPoint != null){
			// logic that works:
			if(clickedPoint1.x < draggedPoint.x && clickedPoint1.y < draggedPoint.y){
				if(fillShapes)
					paintTool.fillRect(clickedPoint1.x, clickedPoint1.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
				else paintTool.drawRect(clickedPoint1.x, clickedPoint1.y,
						Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));

				return;
			}
			// Works! When rectangle is drawn by dragging from point pressed towards lower 
			// left of canvas.
			if(clickedPoint1.x > draggedPoint.x && clickedPoint1.y < draggedPoint.y){
				if(fillShapes)
					paintTool.fillRect(draggedPoint.x, clickedPoint1.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
				else paintTool.drawRect(draggedPoint.x, clickedPoint1.y,
						Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));

				return;
			}
			// When rectangle is drawn by dragging from point pressed towards upper 
			// right of canvas.
			if(clickedPoint1.x < draggedPoint.x && clickedPoint1.y > draggedPoint.y){
				if(fillShapes)
					paintTool.fillRect(clickedPoint1.x, draggedPoint.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
				else paintTool.drawRect(clickedPoint1.x, draggedPoint.y,
						Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));

				return;
			}
			// Works! When rectangle is drawn by dragging from point pressed towards upper 
			// left of canvas.
			if(clickedPoint1.x > draggedPoint.x && clickedPoint1.y > draggedPoint.y){
				if(fillShapes)
					paintTool.fillRect(draggedPoint.x, draggedPoint.y,
							Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));
				else paintTool.drawRect(draggedPoint.x, draggedPoint.y,
						Math.abs(draggedPoint.x - clickedPoint1.x), Math.abs(draggedPoint.y - clickedPoint1.y));

				return;
			} 
		}
	}
	
	//====================================================================================
		/**
		 * Serves as a listener class for DrawingCanvas.
		 * @author Frank Haro
		 */
	//====================================================================================
    private class PointListener implements MouseListener, MouseMotionListener
    {
    	/**
    	 * When user first Presses and holds the mouse on a point on canvas, the x and y coordinate
    	 * is recorded into a point called pressedPoint.
    	 */
    	public void mousePressed(MouseEvent event)
    	{
    		// Logic that works.
//    		if(drawingChoice != 3)
//    		pressedPoint = event.getPoint();
        }
    	
    	/**
    	 * When user lets go of mouse on a point on canvas, the x and y coordinate
    	 * is recorded into a point called releasedPoint.
    	 */
    	public void mouseReleased(MouseEvent event)
    	{	
//    		if(drawingChoice != 3){
//    		releasedPoint = event.getPoint();
//    		dragging = false;	
//    		
//    		// Add appropriate shape.
//
//			 switch(drawingChoice){
//			 //rectangle was selected.
//			 case 0:
//				 addDrawnRectangle(event);
//				 break;
//			 //line was selected.
//			 case 1:
//				 addDrawnLine(event); migrating to the move instead.
//				 break;
//			 //oval was selected.
//			 case 2:
//				 addDrawnOval(event);
//				 break;
//			 }
//    		}
    	}
    	
    	/**
    	 * When user drags the mouse over points on canvas while mouse left button is pressed, the x and y coordinates
    	 * are recorded into a point called draggedPoint. The shape outline is drawn each time creating an animation.
    	 */
    	public void mouseDragged(MouseEvent event)
    	{
//    		if(drawingChoice != 3){
//    		draggedPoint = new Point(event.getPoint().x, event.getPoint().y);
//    		dragging = true;
//    		((DrawingCanvas)event.getSource()).repaint();
//    		}
    	}    	
    	
    	/**
    	 * Tracks the point the user clicks on in the canvas.
    	 */
    	public void mouseClicked(MouseEvent event) {
    		switch(drawingChoice) {
    		// rectangle.
    		case 0:
    			if(clickedPoint1 == null){
    				clickedPoint1 = event.getPoint();
    			}else if(clickedPoint2 == null){
    				clickedPoint2 = event.getPoint();
    				addDrawnRectangle(event);
    			}
    			
    			break;
    		// line.
    		case 1:
    			if(clickedPoint1 == null){
    				clickedPoint1 = event.getPoint();
    			}else if(clickedPoint2 == null){
    				clickedPoint2 = event.getPoint();
    				addDrawnLine(event);
    			}
    				
    			break;
    		// oval.
    		case 2:
    			if(clickedPoint1 == null){
    				clickedPoint1 = event.getPoint();
    			}else if(clickedPoint2 == null){
    				clickedPoint2 = event.getPoint();
    				addDrawnOval(event);
    			}
    			break;
    		// triangle.
    		case 3:
    			if(clickedPoint1 == null){
    				clickedPoint1 = event.getPoint(); clickedPoint2 = null; clickedPoint3 = null;
    			}else if(clickedPoint2 == null){
    				clickedPoint2 = event.getPoint(); clickedPoint3 = null;
    			}else if(clickedPoint3 == null){
    				clickedPoint3 = event.getPoint();
    				addDrawnTriangle(event);	
    			}
    			break;
    		
    		}
    		
    		
    		
    		
    	}
    	
    	//Unused method.
    	public void mouseEntered(MouseEvent event) {}
    	public void mouseExited(MouseEvent event) {}
    	
    	/**
    	 * Tracks the movement of the mouse over the canvas.
    	 */
    	public void mouseMoved(MouseEvent event) {
    		switch(drawingChoice) {
    		// rectangle.
    		case 0:
    			if(clickedPoint1 != null && clickedPoint2 == null){
    				draggedPoint = event.getPoint();
    				dragging = true;
    				((DrawingCanvas)event.getSource()).repaint();
    			}else dragging = false;
    			break;
    		// line.
    		case 1:
    			if(clickedPoint1 != null && clickedPoint2 == null){
    				draggedPoint = event.getPoint();
    				dragging = true;
    				((DrawingCanvas)event.getSource()).repaint();
    			}else dragging = false;
    			break;
    		// oval.
    		case 2:
    			if(clickedPoint1 != null && clickedPoint2 == null){
    				draggedPoint = event.getPoint();
    				dragging = true;
    				((DrawingCanvas)event.getSource()).repaint();
    			}else dragging = false;
    			break;
    		// triangle.
    		case 3:
    			// Only when a user makes the first click must we watch for the mouse moving.
        		if(clickedPoint1 != null && (clickedPoint2 == null || clickedPoint3 == null)){
            		draggedPoint = event.getPoint();
            		dragging = true;
            		((DrawingCanvas)event.getSource()).repaint();
            		}else dragging = false;
    			break;
    		
    		}
    	}

    	/**
    	 * Adds the drawn triangle to the inner array list.
    	 * @param event
    	 */
		private void addDrawnTriangle(MouseEvent event) {
			// TODO Auto-generated method stub
			if(clickedPoint3 != null){
				MyTriangle tri = new MyTriangle(clickedPoint1.x, clickedPoint1.y,
						 clickedPoint2.x, clickedPoint2.y,
						 clickedPoint3.x, clickedPoint3.y, currentColor);
				tri.setFilled(fillShapes);
				shapeList.add(tri);
				clickedPoint1 = clickedPoint2 = clickedPoint3 = null;		
				((DrawingCanvas)event.getSource()).repaint();
			}
			
		}

		/**
		 * Adds rectangle drawn to the inner array list.
		 * @param event
		 */
		public void addDrawnRectangle(MouseEvent event) {
			if(clickedPoint1 != null && clickedPoint2 != null){
				// Logic that works.
				// When rectangle is created by dragging from point pressed to lower right of canvas.
				if(clickedPoint1.x < clickedPoint2.x && clickedPoint1.y < clickedPoint2.y){
					MyRectangle rectangleDrawn = new MyRectangle(clickedPoint1.x, clickedPoint1.y,
							clickedPoint2.x, clickedPoint2.y, currentColor);

					rectangleDrawn.setFilled(fillShapes);
					shapeList.add(rectangleDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}
				// When rectangle is created by dragging from point pressed to lower left of canvas.
				if(clickedPoint1.x > clickedPoint2.x && clickedPoint1.y < clickedPoint2.y){
					MyRectangle rectangleDrawn = new MyRectangle( clickedPoint2.x, clickedPoint1.y,
							clickedPoint1.x, clickedPoint2.y, currentColor);
					rectangleDrawn.setFilled(fillShapes);
					shapeList.add(rectangleDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}
				// When rectangle is created by dragging from point pressed to upper right of canvas.
				if(clickedPoint1.x < clickedPoint2.x && clickedPoint1.y > clickedPoint2.y){
					MyRectangle rectangleDrawn = new MyRectangle( clickedPoint1.x, clickedPoint2.y, 
							clickedPoint2.x, clickedPoint1.y, currentColor);
					rectangleDrawn.setFilled(fillShapes);
					shapeList.add(rectangleDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}
				// When rectangle is created by dragging from point pressed to upper left of canvas.
				if(clickedPoint1.x > clickedPoint2.x && clickedPoint1.y > clickedPoint2.y){
					MyRectangle rectangleDrawn = new MyRectangle( clickedPoint2.x,clickedPoint2.y,
							clickedPoint1.x, clickedPoint1.y, currentColor);
					rectangleDrawn.setFilled(fillShapes);
					shapeList.add(rectangleDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}    	    	
			}
		}

		/**
		 * Adds line drawn to inner array list.
		 * @param event
		 */
		public void addDrawnLine(MouseEvent event) {
			if(clickedPoint1 != null && clickedPoint2 != null){
			MyLine lineDrawn = new MyLine(clickedPoint1.x, clickedPoint1.y, clickedPoint2.x, clickedPoint2.y, currentColor);
			shapeList.add(lineDrawn);
			clickedPoint1 = clickedPoint2 =  null;
			((DrawingCanvas)event.getSource()).repaint();
			}
		}

		/**
		 * Adds oval drawn to the inner array list.
		 * @param event
		 */
		public void addDrawnOval(MouseEvent event) {
//			// When rectangle is created by dragging from point pressed to lower right of canvas.
//			if(pressedPoint.x < releasedPoint.x && pressedPoint.y < releasedPoint.y){
//				MyOval ovalDrawn = new MyOval(pressedPoint.x, pressedPoint.y,
//						releasedPoint.x, releasedPoint.y, currentColor);
//				ovalDrawn.setFilled(fillShapes);
//				shapeList.add(ovalDrawn);
//				((DrawingCanvas)event.getSource()).repaint();
//			}
//			// When oval is created by dragging from point pressed to lower left of canvas.
//			if(pressedPoint.x > releasedPoint.x && pressedPoint.y < releasedPoint.y){
//				MyOval ovalDrawn = new MyOval( releasedPoint.x, pressedPoint.y,
//						pressedPoint.x, releasedPoint.y, currentColor);
//				ovalDrawn.setFilled(fillShapes);
//				shapeList.add(ovalDrawn);
//				((DrawingCanvas)event.getSource()).repaint();
//			}
//			// When oval is created by dragging from point pressed to upper right of canvas.
//			if(pressedPoint.x < releasedPoint.x && pressedPoint.y > releasedPoint.y){
//				MyOval ovalDrawn = new MyOval( pressedPoint.x, releasedPoint.y, 
//						releasedPoint.x, pressedPoint.y, currentColor);
//				ovalDrawn.setFilled(fillShapes);
//				shapeList.add(ovalDrawn);
//				((DrawingCanvas)event.getSource()).repaint();
//			}
//			// When oval is created by dragging from point pressed to upper left of canvas.
//			if(pressedPoint.x > releasedPoint.x && pressedPoint.y > releasedPoint.y){
//				MyOval ovalDrawn = new MyOval( releasedPoint.x,releasedPoint.y,
//						pressedPoint.x, pressedPoint.y, currentColor);
//				ovalDrawn.setFilled(fillShapes);
//				shapeList.add(ovalDrawn);
//				((DrawingCanvas)event.getSource()).repaint();
//			}    		
//			
			
			
			if(clickedPoint1 != null && clickedPoint2 != null){
				// Logic that works.
				// When rectangle is created by dragging from point pressed to lower right of canvas.
				if(clickedPoint1.x < clickedPoint2.x && clickedPoint1.y < clickedPoint2.y){
					MyOval ovalDrawn = new MyOval(clickedPoint1.x, clickedPoint1.y,
							clickedPoint2.x, clickedPoint2.y, currentColor);
					ovalDrawn.setFilled(fillShapes);
					shapeList.add(ovalDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}
				// When rectangle is created by dragging from point pressed to lower left of canvas.
				if(clickedPoint1.x > clickedPoint2.x && clickedPoint1.y < clickedPoint2.y){
					MyOval ovalDrawn = new MyOval( clickedPoint2.x, clickedPoint1.y,
							clickedPoint1.x, clickedPoint2.y, currentColor);
					ovalDrawn.setFilled(fillShapes);
					shapeList.add(ovalDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}
				// When rectangle is created by dragging from point pressed to upper right of canvas.
				if(clickedPoint1.x < clickedPoint2.x && clickedPoint1.y > clickedPoint2.y){
					MyOval ovalDrawn = new MyOval( clickedPoint1.x, clickedPoint2.y, 
							clickedPoint2.x, clickedPoint1.y, currentColor);
					ovalDrawn.setFilled(fillShapes);
					shapeList.add(ovalDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}
				// When rectangle is created by dragging from point pressed to upper left of canvas.
				if(clickedPoint1.x > clickedPoint2.x && clickedPoint1.y > clickedPoint2.y){
					MyOval ovalDrawn = new MyOval( clickedPoint2.x,clickedPoint2.y,
							clickedPoint1.x, clickedPoint1.y, currentColor);
					ovalDrawn.setFilled(fillShapes);
					shapeList.add(ovalDrawn);
					clickedPoint1 = clickedPoint2 = null;
					((DrawingCanvas)event.getSource()).repaint();
					return;
				}    	    	
			}
			
		}
                 
    } //end of PointListener


	
}
