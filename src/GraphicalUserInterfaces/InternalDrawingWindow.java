package GraphicalUserInterfaces;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.beans.PropertyVetoException;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 *  Assignment #: 2
 *  Name: 		  Gabriel
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: InternalDrawingWindow is a JInternalFrame that sets up the an InternalDrawinPanel (jpanel)
 *  and adds it to this container.
 */
@SuppressWarnings("serial")
public class InternalDrawingWindow extends JInternalFrame{
	// Has a JPanel with drawing tools.
	private final InternalDrawingPanel panel = new InternalDrawingPanel();
	private final MainWindow mainParentContainer;
	
	// Dimensions of the window
	private final int width =450, height = 450;
	
	/**
	 * Creates a JInternalFrame with the specified title, resizability, closability, maximizability, and minimizability. 
	 * Also a reference to the MDI Container is passed so that way we can call appropriate methods to update the JMenuBar.
	 * @param title - title of the JInternalFrame being created and added.
	 * @param resizable 
	 * @param closeable
	 * @param maximizable
	 * @param minimizable
	 * @param parentContainer
	 */
	public InternalDrawingWindow(String title, boolean resizable,boolean closeable,boolean maximizable,boolean minimizable, MainWindow parentContainer) {
		super(title, resizable, closeable, maximizable, minimizable);
		this.setPreferredSize(new Dimension(width, height));
		this.add(panel);
		this.mainParentContainer = parentContainer;
	}
	
	/**
	 * 
	 * @return panel of this Internal Drawing Window.
	 */
	public InternalDrawingPanel getPanel(){
		return panel;
	}

	/**
	 * Will call the removeWindowsMenuItem method of the Main container of the MDI so that
	 * way we can remove this JInternalFrame from the list on the windowsMenu.
	 */
	@Override
	public void doDefaultCloseAction() {
		
		// Let us update 
		mainParentContainer.updateWindowsMenuItem(this.title);
		// TODO Auto-generated method stub
		super.doDefaultCloseAction();
		
	}
	
	@Override
	public void setSelected(boolean select) {
		try {
			super.setSelected(select);
			mainParentContainer.updateInternalWinSelected(this);			
		} catch (PropertyVetoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
