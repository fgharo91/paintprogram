package GraphicalUserInterfaces;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *  Assignment #: 2
 *  Name: 		  Gabriel
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: Main Window is a JFrame that is container to hold a JMenubar and
 *  a JDesktopPane.
 */
@SuppressWarnings("serial")
public class MainWindow extends JFrame {	
	// Has a JMenuBar, JDesktopPane,
	private final JMenuBar windowMenus = new JMenuBar();
	private final JDesktopPane windowPane = new JDesktopPane(); 
	private final ArrayList<InternalDrawingWindow> internalWins = new ArrayList<InternalDrawingWindow>();
	
	// An explicit reference to this window.
	private final MainWindow This = this;
	
	// Dimensions of the window
	private final int width = 1000, height = 600;
	
	// Menus fileMenu and windowMenu
	private final JMenu fileMenu = new JMenu("File");
	private final JMenu windowMenu = new JMenu("Windows");
	private final JMenu saveMenu = new JMenu("Save Image");
	
	// fileMenu Items
	private final JMenuItem newWindowMenuItem = new JMenuItem("New"), exitWindowMenuItem = new JMenuItem("Exit");
	
	// saveWindowMenuItem options
	private final JMenuItem saveOption1 = new JMenuItem("gif"),saveOption2 = new JMenuItem("png"),saveOption3 = new JMenuItem("jpg");
	
	// A count of the number of windows.
	private static int internalWindowCount = 0;
	private int indexOfFocusedWin = -1;
	
	/**
	 * Calls the appropriate methods to set up this GUI.
	 */
	public MainWindow(String title) {
		this.setTitle(title);
		setGUIComponents();
		setEventHandlers();
	}
	

	/**
	 * Composes the GUI components in the container MainWindow.
	 */
	private void setGUIComponents() {
		// Compose saveWindowMenuItem to have a menu
		saveMenu.add(saveOption1);
		saveMenu.add(saveOption2);
		saveMenu.add(saveOption3);
		
		// Compose menus for JMenuBar
		fileMenu.add(newWindowMenuItem);
		fileMenu.add(saveMenu);
		fileMenu.add(exitWindowMenuItem);
		
		
		// Compose the JMenuBar
		windowMenus.add(fileMenu);
		windowMenus.add(windowMenu);
		
		// Add main components to MainWindow
		this.setJMenuBar(windowMenus);
		this.add(windowPane);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(width, height));
		this.pack();
	}
	
	
	/**
	 * Sets up the event handlers to the GUI components.
	 */
	private void setEventHandlers() {
		
				
		// set up listener for new menu item
		newWindowMenuItem.addActionListener(
				// anonymous inner class
				new ActionListener() {
					// display new internal window
					@Override
					public void actionPerformed(ActionEvent event) {
						internalWindowCount++;

						String internalWinName = "Picture" + internalWindowCount;

						// Creates a new InternalDrawingWindow to add.
						InternalDrawingWindow drawingframe = new InternalDrawingWindow(
								internalWinName, false, true, false, true, This);
						

						// Causes subcomponents of drawingframe to be laid out to 
						// preffered size.
						drawingframe.pack();

						// attach internal frame.
						windowPane.add(drawingframe);
						internalWins.add(drawingframe);

						// show internal frame.
						drawingframe.setVisible(true);

						// Attach the name of the internal frame
						// to the menu Windows.
						JMenuItem windowItem = new JMenuItem(internalWinName);
						windowItem.addActionListener(new ActionListener(){

							@Override
							public void actionPerformed(ActionEvent e) {
								indexOfFocusedWin = Integer.valueOf(((JMenuItem)e.getSource()).getText().substring("Picture".length(),
										((JMenuItem)e.getSource()).getText().length() ))-1;
								internalWins.get(indexOfFocusedWin).setSelected(true);
							}
							
						});
						
						windowMenu.add(windowItem);
					}


				});
		
		// Save the focused windows drawing with gif.
		saveOption1.addActionListener(
				// anonymous inner class
				new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
					if(indexOfFocusedWin == -1)
						return;
						
						 BufferedImage image = (BufferedImage) internalWins.get(indexOfFocusedWin).getPanel().getCanvas().getSavableImage();
						 internalWins.get(indexOfFocusedWin).getPanel().getCanvas().paint(image.createGraphics());
						 File saveFile = new File(internalWins.get(indexOfFocusedWin).getTitle() + ".gif");
			             JFileChooser chooser = new JFileChooser();
			             chooser.setSelectedFile(saveFile);
			             int rval = chooser.showSaveDialog(null);
			             if (rval == JFileChooser.APPROVE_OPTION) {
			                 saveFile = chooser.getSelectedFile();
			                 
			                 /* Write the filtered image in the selected format,
			                  * to the file chosen by the user.
			                  */
			                 try {
			                     ImageIO.write(image, "gif", saveFile);
			                 } catch (IOException ex) {
			                 }
			             }
					}							
				});
		
		// Save the focused windows drawing with gif.
		saveOption2.addActionListener(
				// anonymous inner class
				new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if(indexOfFocusedWin == -1)
							return;
						
						 BufferedImage image = (BufferedImage) internalWins.get(indexOfFocusedWin).getPanel().getCanvas().getSavableImage();
						 internalWins.get(indexOfFocusedWin).getPanel().getCanvas().paint(image.createGraphics());
						 File saveFile = new File(internalWins.get(indexOfFocusedWin).getTitle() + ".png");
			             JFileChooser chooser = new JFileChooser();
			             chooser.setSelectedFile(saveFile);
			             int rval = chooser.showSaveDialog(null);
			             if (rval == JFileChooser.APPROVE_OPTION) {
			                 saveFile = chooser.getSelectedFile();
			                 
			                 /* Write the filtered image in the selected format,
			                  * to the file chosen by the user.
			                  */
			                 try {
			                     ImageIO.write(image, "png", saveFile);
			                 } catch (IOException ex) {
			                 }
			             }
					}							
				});
		
		// Save the focused windows drawing with gif.
		saveOption3.addActionListener(
				// anonymous inner class
				new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if(indexOfFocusedWin == -1)
							return;
						
						 BufferedImage image = (BufferedImage) internalWins.get(indexOfFocusedWin).getPanel().getCanvas().getSavableImage();
						 internalWins.get(indexOfFocusedWin).getPanel().getCanvas().paint(image.createGraphics());
						 File saveFile = new File(internalWins.get(indexOfFocusedWin).getTitle() + ".jpg");
			             JFileChooser chooser = new JFileChooser();
			             chooser.setSelectedFile(saveFile);
			             int rval = chooser.showSaveDialog(null);
			             if (rval == JFileChooser.APPROVE_OPTION) {
			                 saveFile = chooser.getSelectedFile();
			                 
			                 /* Write the filtered image in the selected format,
			                  * to the file chosen by the user.
			                  */
			                 try {
			                     ImageIO.write(image, "jpg", saveFile);
			                 } catch (IOException ex) {
			                 }
			             }
					}							
				});
		
		
		


		// set up listener for exit menu item
		exitWindowMenuItem.addActionListener(
				// anonymous inner class
				new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						System.exit(0);								
					}							
				});		
	}


	/**
	 * Updates the windowMenu on the JMenuBar of the MainWindow by removing a title to
	 * the JInternalFrame that was deleted from that menu list.
	 */
	public void updateWindowsMenuItem(String title) {
		windowMenu.removeAll();
		
		
		// look for the component with title and remove it.
		for(int i =0;i<internalWins.size();i++) {
			if(internalWins.get(i).getTitle().equals(title)){
				internalWins.remove(i);
				break;
			}
		}
		
		// Refresh the windows menu item.
		for(int i =0;i<internalWins.size();i++) {
			windowMenu.add(internalWins.get(i).getTitle());
		}
		
		// last window deleted.
		if(internalWins.size() == 0)
			indexOfFocusedWin = -1;
	}
	
	public void updateInternalWinSelected(InternalDrawingWindow win) {
		indexOfFocusedWin=internalWins.indexOf(win);
		
	}
}
