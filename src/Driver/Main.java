package Driver;
import GraphicalUserInterfaces.MainWindow;

/**
 *  Assignment #: 2
 *  Name: 		  Frank Haro
 *  StudentID:	  1203216477
 *  Lecture Topic: Inheritance, Advanced GUI's
 *  Description: This is the driver class that starts the program.
 */
public class Main {
	
	public static void main(String[] args) {
		// Start the MainWindow to initialize the app.
		MainWindow mainWin = new MainWindow("Drawing Program");
		mainWin.setVisible(true);
	}

}
